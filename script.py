import os.path
import requests
from requests.auth import HTTPBasicAuth


def send_core_function(suministro, orden, tabla=0):
    """
    Esta es la funcionalidad Core del sistema
    - Recibe los parametros indicados y los envia atravez
    :param suministro: int
    :param orden: int
    :param tabla: int
    :return: True or False
    """

    url = "http://ensanet/netfile/Escaneos.aspx?Tabla=" + str(tabla) + "&Norden=" + str(orden) + "&Suministro=" + str(suministro)
    headers = {}

    fin = open('text.pdf', 'rb')
    files = {'file': fin}

    try:
        r = requests.post(
            url,
            files=files,
            auth=HTTPBasicAuth('user', 'password')
        )
        return r.text
    finally:
        fin.close()


print(send_core_function(123, 456))
